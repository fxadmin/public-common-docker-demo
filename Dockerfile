FROM alpine:latest as BUILDER

ENV DEMO_VERSION=384c387ce6766f241f75301c9a82da8fb2b25990

ADD http://35.186.216.115/repository/artifacts/demo/${DEMO_VERSION}/demo-${DEMO_VERSION} /demo

RUN chmod +x /demo

FROM alpine:latest

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

COPY --from=BUILDER /demo /demo

ENTRYPOINT ["/demo"]

LABEL "maintainer"="cloudsquad@fxinnovation.com" \
      "org.label-schema.name"="demo" \
      "org.label-schema.base-image.name"="scratch" \
      "org.label-schema.base-image.version"="latest" \
      "org.label-schema.description"="demo in a container" \
      "org.label-schema.url"="https://bitbucket.org/fxadmin/fxinnovation-common-application-demo" \
      "org.label-schema.vcs-url"="https://bitbucket.org/fxadmin/public-common-docker-demo" \
      "org.label-schema.vendor"="FXinnovation" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.vcs-ref"=$VCS_REF \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.test"="this is a test label" \
      "org.label-schema.usage"="docker run fxinnovation/demo:[TAG]"
